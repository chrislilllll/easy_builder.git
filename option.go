package easy_builder

import "gitee.com/chrislilllll/easy_builder.git/helper"

type Option func(*option)
type M map[string]interface{}

type option struct {
	wheres   map[string]M
	orderBy  map[string]string
	page     int64
	pageSize int64
}

// WithWhere where eq
func WithWhere(field string, value interface{}) Option {
	return buildCond(helper.EQ, field, value)
}

// WithWhereNot where子句 not
func WithWhereNot(condN, field string, value interface{}) Option {
	return func(opt *option) {
		if opt.wheres == nil {
			opt.wheres = make(map[string]M)
		}
		child := make(M)
		child[condN] = value
		opt.wheres[field] = child
	}
}

func WithWhereNotIn(field string, value interface{}) Option {
	return buildCond(helper.NEI, field, value)
}

func WithWhereIn(field string, value interface{}) Option {
	return buildCond(helper.IN, field, value)
}

func WithWhereLike(field string, value interface{}) Option {
	return buildCond(helper.LIKE, field, value)
}

func buildCond(ck helper.CondEx, field string, value interface{}) Option {
	return func(opt *option) {
		if opt.wheres == nil {
			opt.wheres = make(map[string]M)
		}
		child := make(M)
		child[string(ck)] = value
		opt.wheres[field] = child
	}
}

// WithOrderBy  排序
func WithOrderBy(field, sort string) Option {
	return func(opt *option) {
		if opt.orderBy == nil {
			opt.orderBy = make(map[string]string)
		}
		opt.orderBy[field] = sort
	}
}

// WithLimit 分页
func WithLimit(page, pageSize int64) Option {
	return func(opt *option) {
		opt.page = page
		opt.pageSize = pageSize
	}
}

func getOption() *option {
	return new(option)
}
