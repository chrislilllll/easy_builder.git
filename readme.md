
## 关于
基于Gorm的简单询封装的查询器

特点：

    简单抽象了通用的first、get、pageList,使得开发者可以专注业务

## 安装
```
go get -u gitee.com/chrislilllll/easy_builder.git
```

## 使用

```go

    type User struct {
        Id     int
        Name   string
        Gender string
        Hobby  string
    }

    var usr User

    //where not
    qb := New(WithWhereNot(">=", "name", "jack"))
    err := qb.First(db, &usr)
    
    //where in
    qb := New(WithWhereIn("name", []string{"jack"}))
    err := qb.First(db, &usr)
    
    //where like
    qb := New(WithWhereLike("name", "%jack%"))
    err := qb.First(db, &usr)


    //list
    
    var users []User
    qb := New(WithWhere("name", "jack"))
    err := qb.Get(db, &users)
    
    //page list
    qb := New(WithWhereNot("<>", "name", "jack"), WithLimit(3, 1))
    err := qb.List(db, &users)

    //指定列数据
    qb := New(WithWhere("name", "jack"))
    err := qb.Get(db, &users, "hobby")
	
```



