package easy_builder

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"testing"
)

type Userinfos struct {
	Id     int
	Name   string
	Gender string
	Hobby  string
}

var db *gorm.DB

func init() {
	cdb, err := connMySQL()
	if err != nil {
		panic(err)
	}
	db = cdb
}
func connMySQL() (*gorm.DB, error) {

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=%t&loc=%s",
		"root",
		"12345678",
		"127.0.0.1:3306",
		"demo",
		true,
		"Local")

	return gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}

func TestFirst(t *testing.T) {

	var usr Userinfos
	qb := New(db, WithWhereNot(">=", "name", "jack"))

	err := qb.First(&usr, "hobby")
	if err != nil {
		t.Error()
	}

	t.Log(usr)
}

func TestGet(t *testing.T) {

	var usr []Userinfos
	qb := New(db, WithWhereNot("<>", "name", "jack"))

	err := qb.Get(&usr)

	if err != nil {
		t.Error(err)
	}

	t.Log(usr)
}

func TestList(t *testing.T) {

	var usr []Userinfos
	qb := New(db,
		WithWhereNot("<>", "name", "jack"),
		WithLimit(3, 1),
	)

	err := qb.List(&usr)

	if err != nil {
		t.Error(err)
	}

	t.Log(usr)
}
