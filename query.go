package easy_builder

import (
	"errors"
	"fmt"
	"gitee.com/chrislilllll/easy_builder.git/helper"
	"gorm.io/gorm"
	"reflect"
	"strings"
)

type QueryBuilder struct {
	Db  *gorm.DB
	opt *option
}

func New(db *gorm.DB, options ...Option) QueryBuilder {

	opt := getOption()
	for _, f := range options {
		f(opt)
	}
	return QueryBuilder{
		opt: opt,
		Db:  db,
	}
}

// FirstById id查询一条数据
func (qb QueryBuilder) FirstById(instance interface{}, id int64, columns ...string) error {
	return qb.Db.Model(instance).Where("id = ?", id).Select(columns).First(instance).Error
}

// First 查询单条记录
func (qb QueryBuilder) First(instance interface{}, columns ...string) error {
	var query gorm.DB
	err := qb.base(instance, &query)
	if err != nil {
		return err
	}
	return query.Select(columns).First(instance).Error
}

// Get 查询多条记录
func (qb QueryBuilder) Get(instance interface{}, columns ...string) error {
	var query gorm.DB
	err := qb.base(instance, &query)
	if err != nil {
		return err
	}
	return query.Select(columns).Find(instance).Error
}

// List 分页查询多条记录
func (qb QueryBuilder) List(instance interface{}, columns ...string) error {
	var query gorm.DB
	err := qb.base(instance, &query)
	if err != nil {
		return err
	}
	return query.Select(columns).Scopes(Paginate(qb.opt.page, qb.opt.pageSize)).Find(instance).Error
}

// Create 创建
func (qb QueryBuilder) Create(instance interface{}) error {
	return qb.Db.Model(instance).Create(instance).Error
}

// Update 根据条件修改单列的值
func (qb QueryBuilder) Update(instance interface{}, column string, value interface{}) error {
	var query gorm.DB
	err := qb.base(instance, &query)
	if err != nil {
		return err
	}
	return query.Update(column, value).Error
}

// Updates 根据条件修改多列的值
func (qb QueryBuilder) Updates(instance interface{}, values interface{}) error {
	var query gorm.DB
	err := qb.base(instance, &query)
	if err != nil {
		return err
	}
	return query.Updates(values).Error
}

func Paginate(page, pageSize int64) func(db *gorm.DB) *gorm.DB {

	return func(db *gorm.DB) *gorm.DB {
		if page == 0 {
			page = 1
		}

		switch {
		case pageSize > 100:
			pageSize = helper.DefaultPageMaxSize
		case pageSize <= 0:
			pageSize = helper.DefaultPageSize
		}

		offset := (page - 1) * pageSize
		return db.Offset(int(offset)).Limit(int(pageSize))
	}
}

func (qb QueryBuilder) assemble(query *gorm.DB) error {

	//组装where
	for field, v := range qb.opt.wheres {

		for c, vv := range v {
			template := "%s %s ?"
			switch helper.CondEx(c) {
			case helper.EQ, helper.NE, helper.LT, helper.LTE, helper.GT, helper.GTE, helper.LIKE, helper.NOT:
				query = query.Where(fmt.Sprintf(template, field, c), vv)
			case helper.IN, helper.NEI:
				rt := reflect.TypeOf(vv)
				if rt.Kind() != reflect.Slice {
					return fmt.Errorf("%v data type is not slice", vv)
				}
				query = query.Where(fmt.Sprintf(template, field, "in"), vv)
			default:
				return errors.New("wrong where expression")
			}
		}
	}

	//组装order by
	for field, sort := range qb.opt.orderBy {
		if strings.ToUpper(sort) != "ASC" || strings.ToUpper(sort) != "DESC" {
			return errors.New("wrong sort rule")
		}
		query.Order(fmt.Sprintf("%s %s", field, sort))
	}
	return nil
}

func (qb QueryBuilder) base(instance interface{}, query *gorm.DB) error {
	if qb.opt == nil {
		return errors.New("option is null")
	}
	query = qb.Db.Model(instance)
	err := qb.assemble(query)
	return err
}
