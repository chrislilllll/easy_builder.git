package helper

type CondEx string

const (
	IN   CondEx = "in"
	EQ   CondEx = "="
	NEI  CondEx = "not in"
	LIKE CondEx = "like"
	NE   CondEx = "!="
	NOT  CondEx = "<>"
	LT   CondEx = "<"
	LTE  CondEx = "<="
	GT   CondEx = ">"
	GTE  CondEx = ">="

	// DefaultPageSize 分页大小
	DefaultPageSize = 10
	// DefaultPageMaxSize   最大分页大小
	DefaultPageMaxSize = 100
)
